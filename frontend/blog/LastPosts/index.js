import { getSortedPostsData } from '../../../lib/blogPosts';

const LastPosts = ({allPostsData}) => {
  console.log("XCCC", {allPostsData});
  return (<div>
    <h2>Poslední příspěvky</h2>
  </div>);
}

export async function getStaticProps() {
  const allPostsData = getSortedPostsData();
  return {
    props: {
      allPostsData
    }
  };
};

export default LastPosts;
