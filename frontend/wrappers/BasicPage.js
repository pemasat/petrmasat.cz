import MainMenu from './components/MainMenu';

const BasicPage = ({children}) => {
  return (<div>
    <MainMenu />
    {children}
  </div>);
};

export default BasicPage;
