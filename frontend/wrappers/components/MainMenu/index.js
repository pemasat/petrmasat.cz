import Link from 'next/link';

const MainMenu = () => {

  return (<nav>
    <Link href="/"><a>Home</a></Link>
    <Link href="/about-me"><a>About me</a></Link>
  </nav>);
};

export default MainMenu;
